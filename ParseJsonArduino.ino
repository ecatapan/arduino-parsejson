#include <ESP8266WiFi.h> //library to be able to connect the ESP8266 to a WiFi network
#include "ArduinoJson.h" //helps you work with JSON objects on Arduino
#include <ESP8266HTTPClient.h>  //ESP8266HTTPClient.h to be able to make HTTP requests.



int hostId = 192; //your ip 192.168.1.192
int mId = 1000392;  // machine id requested in api
int httpResponseCode;
String api = "http://192.168.1.150:8080/joborder/IoTCheckWorkQueue"; // your api
String ssid = "Wifi_Er";// Put your SSID
String password = "wmdcwifier"; //your wifi password
String response;
HTTPClient http;

void setup() {
  
  // put your setup code here, to run once:

  Serial.begin (115200);
  WiFi.begin(ssid, password);
  IPAddress ip(192,168,1,hostId);
  IPAddress gateway(192,168,1,100);
  IPAddress subnet(255,255,255,0);
  IPAddress dns(192,168,1,100);
  WiFi.config(ip, gateway, subnet,dns);
  
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    delay(500);
    ESP.restart();
  }
  
 
}
void inithttp() {  
  if(WiFi.status()== WL_CONNECTED) {
    http.begin(api);
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    char params[128];
    snprintf(params, sizeof params, "%s%d", "uId=2&h=3&fw=4&timestamp=5&mId=", mId);
    httpResponseCode = http.POST(params);

    if (httpResponseCode > 0) {
      response = http.getString();
      http.end();
    }
    
  }
}


void loop() {
  delay(5000);
  inithttp();
  // put your main code here, to run repeatedly:
     while (!Serial) continue;
  StaticJsonDocument<256> doc;
  deserializeJson(doc, response);

  const char* joInfo = doc["joInfo"]; // "27638-106054"
  int success = doc["success"]; // 1
  long dTime = doc["dTime"]; // 1632826681
  int mId = doc["mId"]; // 10007
  long startTime = doc["startTime"]; // 1632752881
  const char* hash = doc["hash"]; // "sgkdfkjt45-2340-8gasdf|fjsdkflksj==" 

  Serial.print("joInfo: ");
  Serial.println(joInfo);
  Serial.print("success: ");
  Serial.println(success);
  Serial.print("dTime: ");
  Serial.println(dTime);
  Serial.print("startTime: ");
  Serial.println(startTime);
  Serial.print("hash: ");
  Serial.println(hash);
}
